from django.test import TestCase, Client
from django.urls import resolve
from .views import index
# Create your tests here.


class ProfilPerusahaan(TestCase):
	def test_profil_perusahaan_is_exist(self):
		response = Client().get('/profil-perusahaan/')
		self.assertEqual(response.status_code,200)

	def test_using_index_func(self):
		found = resolve('/profil-perusahaan/')
		self.assertEqual(found.func,index)