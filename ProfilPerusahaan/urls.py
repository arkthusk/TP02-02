from django.conf.urls import url
from .views import index, profil_perusahaan

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^profil_perusahaan/$', profil_perusahaan, name = 'profil_perusahaan')
]
