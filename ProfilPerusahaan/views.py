from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
# Create your views here.
response = {}

# Create your views here.

def index(request):
    print ("#==> masuk index")
    if 'user_login' in request.session:
        return HttpResponseRedirect(reverse('profil-perusahaan:profil_perusahaan'))
    else:
        html = 'daftar_login.html'
        return render(request, html, response)


def profil_perusahaan(request):
    print ("#==> profile")
    ## sol : bagaimana cara mencegah error, jika url profile langsung diakses
    if 'user_login' not in request.session.keys():
        return HttpResponseRedirect(reverse('profil-perusahaan:index'))
    ## end of sol
    html = 'profil-perusahaan.html'
    return render(request, html, response)