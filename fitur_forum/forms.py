from django import forms

class Forum_Lowongan(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    lowongan_attrs = {
        'type': 'text',
        'cols': 50,
        'rows': 4,
        'class': 'todo-form-textarea',
        'placeholder':'Masukan deskripsi...'
    }

    lowongan = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=lowongan_attrs))
