from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Forum_Lowongan
from .models import Forum
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


# Create your views here.
response = {}
def index(request):
    forum = Forum.objects.all().order_by('-created_date')
    
    #pagination
    paginator = Paginator(forum, 3)
    page = request.GET.get('page')
    try:
        halaman = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        halaman = paginator.page(1)


    response['Forum'] = halaman
    html = 'fitur_forum/fitur_forum.html'
    response['Forum_Lowongan'] = Forum_Lowongan
    return render(request, html, response)

def add_forum(request):
    form = Forum_Lowongan(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['lowongan'] = request.POST['lowongan']
        forum = Forum(lowongan=response['lowongan'])
        forum.save()
        return HttpResponseRedirect('/fitur-forum/')
    else:
        return HttpResponseRedirect('/fitur-forum/')
