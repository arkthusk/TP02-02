from django.apps import AppConfig


class FiturLoginConfig(AppConfig):
    name = 'fitur_login'
