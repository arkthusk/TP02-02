from django.test import TestCase
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index


# Create your tests here.
class fitur_login(TestCase):
    def test_fitur_login_is_exist(self):
        response = Client().get('/fitur-login/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/fitur-login/')
        self.assertEqual(found.func, index)


    

