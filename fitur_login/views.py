from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from .models import User
from fitur_forum.models import Forum
# Create your views here.
response={}
def index(request):
    response['author'] = "Valerysa Regita" #TODO Implement yourname
    html = 'daftar_login.html'
    print(User.objects.all().values_list('name', flat=True) )
    response['forum']=Forum.objects.all()
    return render(request, html, response)

def add_session(request):
    if request.method == 'POST':
        name = request.POST['name']
        id = request.POST['id']
        request.session['user_login'] = id
        request.session['name'] = name
        try:
            user = User.objects.get(id = id)
        except Exception as e:
            user = User()
            user.id = id
            user.name = name
            user.save()
        return HttpResponseRedirect(reverse('fitur-login:index'))

def remove_session(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('fitur-login:index'))

def add_company(request):
    if request.method == 'POST':
        company_name = request.POST['name']
        id = request.POST['id']
        request.session['company_login'] = id
        request.session['company_name'] = company_name
        return HttpResponseRedirect(reverse('fitur-login:index'))
