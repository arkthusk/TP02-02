"""tp2ppw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import fitur_login.urls as fitur_login
import ProfilPerusahaan.urls as ProfilPerusahaan
import fitur_forum.urls as fitur_forum
from django.views.generic.base import RedirectView


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', RedirectView.as_view(url='/fitur-login/', permanent=True), name='login'),
    url(r'^fitur-login/', include(fitur_login, namespace='fitur-login')),
    url(r'^profil-perusahaan/', include(ProfilPerusahaan, namespace='profil-perusahaan')),
    url(r'^fitur-forum/', include(fitur_forum, namespace='forum')),
]
